"""
Defines functions to to handle common imports which conflict between Python 2 and 3.
"""

import abc
from sys import version_info


def get_ABC():
    """
    Returns correct import of ABC meta class based on used python version.

    :return:
    """

    if version_info >= (3, 4):
        ABC = abc.ABC
    else:
        ABC = abc.ABCMeta('ABC', (), {})

    return ABC


def get_ABC_container():
    """
    Returns correct import of ABC collections based on used python version.

    :return:
    """

    if version_info >= (3, 0):
        import collections.abc
        container_abcs = collections.abc
    else:
        import collections
        container_abcs = collections
    return container_abcs
