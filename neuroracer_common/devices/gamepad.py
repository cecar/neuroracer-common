"""
This module contains everything which is needed to convert the input from the
standard ros node into a JoyInput object. JoyInput has the advantage that is
mapping independent and therefor more flexible. To convert the ros input you
have to instantiate JosMsgMapper and call the remap_raw_input function with the
msg object from the node.
"""

from abc import abstractmethod

from neuroracer_common.actions import ActionTransformerRacerTwist
from neuroracer_common.errors import MissingFieldConfigError, WrongTypeConfigError
from neuroracer_common.version_solver import get_ABC

ABC = get_ABC()

missing_required_field_error = "The config dict is missing a \"{}\" field! It must contain all of the following " \
                               "fields: {}"
missing_field_error = "A mapping entry must have a {} field! It's missing for entry {}!"
wrong_field_type_error = "Type of field {} is wrong! It should be {} but is {}!"
partial_pair_error = "Either none or all parts of a pair field have to be supplied! Got {} expected {}!"


class JoyInput:
    """
    Gamepad input wrapper class with helper functions to identify what state
    should be changed.
    """

    def __init__(self, steer, drive, activate_ai, deactivate_ai,
                 activate_collector, deactivate_collector, dead_man_switch):
        """
        :param steer: steer value between -1 and 1
        :type steer: float
        :param drive: drive value between -1 and 1
        :type drive: float
        :param activate_ai: ai activation button pressed
        :type activate_ai: bool
        :param deactivate_ai: ai deactivation button pressed
        :type deactivate_ai: bool
        :param activate_collector: collector activation button pressed
        :type activate_collector: bool
        :param deactivate_collector: collector deactivation button pressed
        :type deactivate_collector: bool
        :param dead_man_switch: dead man switch pressed
        :type dead_man_switch: bool
        """

        self._steer = steer
        self._drive = drive
        self._activate_ai = activate_ai
        self._deactivate_ai = deactivate_ai
        self._activate_collector = activate_collector
        self._deactivate_collector = deactivate_collector
        self._dead_man_switch = dead_man_switch
        self._action_transformer = ActionTransformerRacerTwist(max_drive=1)

    def has_ai_state_changed(self, other):
        """
        Returns true if the ai state has changed in comparison to other.

        :param other: The other JoyInput to compare with
        :type other: JoyInput
        :returns: True if the ai state has changed from self to other
        :rtype: bool
        """

        if other is None:
            return True

        return self.is_a_ai_button_pressed() and not other.is_a_ai_button_pressed()

    def has_collector_state_changed(self, other):
        """
        Returns true if the collector state has changed in comparison to other.

        :param other: The other JoyInput to compare with
        :type other: JoyInput
        :returns: True if the collector state has changed from self to other
        :rtype: bool
        """

        if other is None:
            return True

        return self.is_a_collector_button_pressed() and not other.is_a_collector_button_pressed()

    def is_a_ai_button_pressed(self):
        """
        Checks if one of the ai buttons is pressed.

        :return: True if at least one button is pressed
        :rtype: bool
        """

        return self._activate_ai or self._deactivate_ai or self._dead_man_switch

    def is_a_collector_button_pressed(self):
        """
        Checks if one of the collector buttons is pressed.

        :return: True if at least one button is pressed
        :rtype: bool
        """

        return self._activate_collector or self._deactivate_collector

    def is_ai_active(self):
        """
        Checks if the ai should get activated.

        :return: True if only the activate ai buttons is pressed
        :rtype: bool
        """

        if self._dead_man_switch or (self._activate_ai and self._deactivate_ai):
            return False
        elif self._activate_ai:
            return True

        return False

    def is_collector_active(self):
        """
        Checks if the collector should get activated.

        :return: True if only the activate ai buttons is pressed
        :rtype: bool
        """

        if self._activate_collector and self._deactivate_collector:
            return False
        elif self._activate_collector:
            return True

        return False

    def is_dead_man_switch_pressed(self):
        """
        Checks if the dead man switch is pressed.

        :return: True if it is pressed
        :rtype: bool
        """

        return self._dead_man_switch

    def get_manual_action(self):
        """
        Constructs and returns a ros msg object for the manual action.

        :return: action object based on the gamepad input
        :rtype: Any
        """

        return self._action_transformer.create_action(self.get_steer(), self.get_drive())

    def get_steer(self):
        """
        Gets the steer value.

        :return: steer value
        :rtype: float
        """

        if self._dead_man_switch:
            return self._steer

        return 0.0

    def get_drive(self):
        """
        Gets the drive value.

        :return: drive value
        :rtype: float
        """

        if self._dead_man_switch:
            return self._drive

        return 0.0

    def __repr__(self):
        return "steer: {}, drive: {}, activate_ai: {}, deactivate_ai: {}, " \
               "activate_collector: {}, deactivate_collector: {}, " \
               "dead_man_switch: {}".format(self._steer,
                                            self._drive,
                                            self._activate_ai,
                                            self._deactivate_ai,
                                            self._activate_collector,
                                            self._deactivate_collector,
                                            self._dead_man_switch)


class JoyMsgMapper:
    """
    Mapper from ros nodes to JoyInput objects.
    """

    def __init__(self, entries):
        """
        :param entries: mapping entries which are used by the mapper
        :type entries: list[AbstractEntry]
        """

        self._entries = entries

    def remap_raw_input(self, raw_joy_input):
        """
        Remaps the given msg to a JoyInput.

        :param raw_joy_input: ros node msg
        :type raw_joy_input: sensor_msgs.msg.Joy
        :return: new JoyInput instance
        :rtype: JoyInput
        """

        remapped_values = {}
        for entry in self._entries:
            remapped_values[entry._name] = entry.remap_raw_input(raw_joy_input=raw_joy_input)

        return JoyInput(**remapped_values)

    @staticmethod
    def _is_field_valid(name, entry, field_name, field_type=None, return_errors=False):
        """
        Helper function to check if the fields are valid. If not it either
        throws exceptions or return a list of them. This behaviour can be
        configured with the return_errors param.

        :param name: name of the entry
        :type name: str
        :param entry: dictionary of the entry (from config server)
        :type entry: Dict
        :param field_name: name of the field to check
        :type field_name: str
        :param field_type: optional parameter which also checks for the supplied type
        :type field_type: types.TypeType
        :param return_errors: returns the errors which would have been thrown if return_errors would be False
        :type return_errors: bool
        :return: True if the field is valid
        :rtype: Union[bool, Tuple[bool, List[Error]]]
        """

        errors = []
        if field_name not in entry:
            errors.append(MissingFieldConfigError(missing_field_error.format("index", name)))
        if field_type is not None:
            if field_name in entry and type(entry[field_name]) is not field_type:
                errors.append(WrongTypeConfigError(
                    wrong_field_type_error.format(field_name, field_type, type(entry[field_name]))))

        if not return_errors:
            if len(errors) != 0:
                raise errors[0]
            else:
                return True

        return len(errors) == 0, errors

    @staticmethod
    def _field_pair_exists(entry, field_names):
        """
        Helper function to check if a field pair exists. For example min and
        max. If a pair only partially exists a ValueError is thrown.

        :param entry: entry for which to check
        :type entry: Dict
        :param field_names: names of the fields which belong to the pair
        :type field_names: List[str]
        :return: True if the pair exists
        :rtype: bool
        """

        found_names = []
        for name in field_names:
            if name in entry:
                found_names.append(name)

        if 0 < len(found_names) < len(field_names):
            raise MissingFieldConfigError(partial_pair_error.format(found_names, field_names))

        return len(found_names) == len(field_names)

    @staticmethod
    def _create_axis_entry(name, entry):
        """
        Helper function to load a new AxisEntry.

        :param name: name of the axis entry
        :type name: str
        :param entry: entry for which to load a AxisEntry
        :type entry: Dict
        :return: new AxisEntry instance
        :rtype: AxisEntry
        """

        JoyMsgMapper._is_field_valid(name, entry, "index", int)
        index = entry["index"]
        min_value = max_value = target_min_value = target_max_value = None
        if JoyMsgMapper._field_pair_exists(entry, ["min", "max"]):
            JoyMsgMapper._is_field_valid(name, entry,
                                         "min")  # todo: it is not allways a float (no point makes it a int)
            JoyMsgMapper._is_field_valid(name, entry, "max")
            min_value = entry["min"]
            max_value = entry["max"]

            if JoyMsgMapper._field_pair_exists(entry, ["target_min", "target_max"]):
                JoyMsgMapper._is_field_valid(name, entry, "min")
                JoyMsgMapper._is_field_valid(name, entry, "max")
                target_min_value = entry["target_min"]
                target_max_value = entry["target_max"]

        return AxisEntry(name=name, index=index, min_value=min_value, max_value=max_value,
                         target_min_value=target_min_value, target_max_value=target_max_value)

    @staticmethod
    def _create_combined_axis_entry(name, entry):
        """
        Helper function to load a new CombinedAxisEntry.

        :param name: name of the combined axis entry
        :type name: str
        :param entry: entry for which to load a CombinedAxisEntry
        :type entry: Dict
        :return: new CombinedAxisEntry instance
        :rtype: CombinedAxisEntry
        """

        JoyMsgMapper._is_field_valid(name, entry, "parts", list)
        part_entries = entry["parts"]
        axis_entries = []
        for index, part in enumerate(part_entries):
            part_name = "{}-{}".format(name, index)
            axis_entries.append(JoyMsgMapper._create_axis_entry(name=part_name, entry=part))

        return CombinedAxisEntry(name=name, axis_entries=axis_entries)

    @staticmethod
    def _create_button_entry(name, entry):
        """
        Helper function to load a new ButtonEntry.

        :param name: name of the combined axis entry
        :type name: str
        :param entry: entry for which to load a ButtonEntry
        :type entry: Dict
        :return: new ButtonEntry instance
        :rtype: ButtonEntry
        """

        JoyMsgMapper._is_field_valid(name, entry, "index", int)

        return ButtonEntry(name=name, index=entry["index"])

    @staticmethod
    def _required_fields_exist(joy_mapping_dict):
        """
        Checks if all required fields exist in the given mapping dict and
        throws an error if this is not the case.

        :param joy_mapping_dict:
        :type joy_mapping_dict: Dict[str, Dict]
        :return: True if all fields have been found
        :rtype: bool
        """

        required_fields = [
            "dead_man_switch",
            "activate_ai",
            "deactivate_ai",
            "activate_collector",
            "deactivate_collector",
            "drive",
            "steer"
        ]

        for field in required_fields:
            if field not in joy_mapping_dict:
                raise MissingFieldConfigError(missing_required_field_error, field, required_fields)

        return True

    @staticmethod
    def from_config(joy_mapping_dict):
        """
        Factory method for the JoyMsgMapper. It validates the received mapping
        dict and raises Errors if it finds errors in it.

        :param joy_mapping_dict: mapping dictionary for the gamepad buttons
        :type joy_mapping_dict: Dict[str, Dict]
        :return: new JoyMsgMapper instance
        :rtype: JoyMsgMapper
        """

        JoyMsgMapper._required_fields_exist(joy_mapping_dict=joy_mapping_dict)

        entries = []

        for name, entry in joy_mapping_dict.items():
            JoyMsgMapper._is_field_valid(name, entry, "type", str)

            entry_type = entry["type"].lower()
            if entry_type == "axis":
                entries.append(JoyMsgMapper._create_axis_entry(name=name, entry=entry))
            elif entry_type == "combined_axis":
                entries.append(JoyMsgMapper._create_combined_axis_entry(name=name, entry=entry))
            elif entry_type == "button":
                entries.append(JoyMsgMapper._create_button_entry(name=name, entry=entry))

        return JoyMsgMapper(entries)


class AbstractEntry(ABC):
    """
    Abstract base class for all Entry types. It is used to save the
    configuration of the entry and to handle the mapping for its own entry.
    """

    def __init__(self, name):
        """
        :param name:  name of the Entry
        :type name: str
        """

        super(AbstractEntry, self).__init__()
        self._name = name

    @abstractmethod
    def remap_raw_input(self, raw_joy_input):
        """
        Remaps the configured field from the raw_joy_input and returns it.

        :param raw_joy_input: raw ros jos msg instance
        :type raw_joy_input: sensor_msgs.msg.Joy
        :return: remapped value
        :rtype: Union[float, bool]
        """

        pass


class AxisEntry(AbstractEntry):
    """
    Entry class for Axis types.
    """

    def __init__(self, name, index, min_value, max_value, target_min_value, target_max_value):
        """
        :param name: name of the entry
        :type name: str
        :param index: index inside the ros msg
        :type index: int
        :param min_value: smallest possible value
        :type min_value: float
        :param max_value: highest possible value
        :type max_value: float
        :param target_min_value: smallest possible target value
        :type target_min_value: float
        :param target_max_value: smallest possible target value
        :type target_max_value: float
        """

        super(AxisEntry, self).__init__(name)
        self._index = index
        self._min_value = min_value
        self._max_value = max_value
        self._target_min_value = target_min_value
        self._target_max_value = target_max_value

        self._clamp_values = min_value is not None and max_value is not None
        self._remap_values = target_min_value is not None and target_max_value is not None

    def remap_raw_input(self, raw_joy_input):
        mapped_value = raw_joy_input.axes[self._index]

        if self._clamp_values:
            # we have to check the boundaries to get the real min and max.
            # otherwise a negative scale breaks this system
            clamp_min = min(self._min_value, self._max_value)
            clamp_max = max(self._min_value, self._max_value)
            mapped_value = min(max(mapped_value, clamp_min), clamp_max)

            if self._remap_values:
                mapped_value = remap_numbers(mapped_value,
                                             self._min_value,
                                             self._max_value,
                                             self._target_min_value,
                                             self._target_max_value)

        return mapped_value


class ButtonEntry(AbstractEntry):
    """
    Entry class for Button types.
    """

    def __init__(self, name, index):
        """
        :param name: name of the entry
        :type name: str
        :param index: index of the button in the ros msg object
        :type index: int
        """
        super(ButtonEntry, self).__init__(name)
        self._index = index

    def remap_raw_input(self, raw_joy_input):
        return raw_joy_input.buttons[self._index] == 1


class CombinedAxisEntry(AbstractEntry):
    """
    Entry class for CombinedAxis types. Currently the combination only supports
    the sum of the found axes. This could be improved in future version but
    works for the current use case.
    """

    def __init__(self, name, axis_entries):
        """
        :param name: name of the entry
        :type name: str
        :param axis_entries: list of AxisEntry instances which should get
                combined into a meta axis
        :type axis_entries: list[AxisEntry]
        """

        super(CombinedAxisEntry, self).__init__(name)
        self._axis_entries = axis_entries

    def remap_raw_input(self, raw_joy_input):
        remapped_values = []
        for index, entry in enumerate(self._axis_entries):
            remapped_values.append(entry.remap_raw_input(raw_joy_input=raw_joy_input))

        return sum(remapped_values)


def remap_numbers(value, old_min, old_max, new_min, new_max):
    """
    Small helper function which maps a value from one number range to a another
    one.

    :param value: value which gets remapped
    :type value: float
    :param old_min: min boundary of the old range
    :type old_min: float
    :param old_max: max boundary of the old range
    :type old_max: float
    :param new_min: min boundary of the new range
    :type new_min: float
    :param new_max: max boundary of the new range
    :type new_max: float
    :return: remapped value
    :rtype: float
    """

    old_span = old_max - old_min
    new_span = new_max - new_min
    return (value - old_min) * new_span / old_span + new_min
