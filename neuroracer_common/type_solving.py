"""
Defines functions to resolve a topic type based on the topic name.
"""

import importlib
import time

import rostopic
from geometry_msgs.msg import TwistStamped
from sensor_msgs.msg import Joy, CompressedImage, Image, LaserScan
from nav_msgs.msg import Odometry

TOPIC_RESOLVE_INTERVAL = 0.2
TOPIC_RESOLVE_TIMEOUT_DEFAULT = 5

#####################################################################################
# In TOPIC_TYPES you can manually resolve topic names to the corresponding type.
# If the type is not provided in TOPIC_TYPES it tries to resolve the type at runtime.
#
# ONLY ADD DEFAULT TOPICS HERE TO PREVENT A RUNTIME ERROR IF THE PUBLISHER IS LOADED
# AFTER THE SUBSCRIBER INITIALIZATION.
#####################################################################################

TOPIC_TYPES = {
    # SIMULATION
    "/camera/zed/rgb/image_rect_color/compressed": CompressedImage,
    "/depth_camera/rgb/image_raw/compressed": CompressedImage,
    "/depth_camera/depth/image_raw": Image,
    "/pf/pose/odom": Odometry,
    "/vesc/odom": Odometry,
    "/scan": LaserScan,
    # ROBOT
    "camera/front/image_rect_color/compressed": CompressedImage,
    "/zed/left/image_raw_color/compressed": CompressedImage,
    "/zed/right/image_raw_color/compressed": CompressedImage,
    "/nr/engine/input/actions": TwistStamped,
    # COMMON
    "/joy": Joy,
}


def _try_resolve_topic_type_string(topic, timeout=0):
    """
    Tries to resolve the string of the type of the specified topic by using the rostopic API.

    :param topic: The topic to resolve
    :type topic: str
    :param timeout: Number of seconds to retry until raise.
    :type timeout: float

    :returns: The string of the topic type
    :rtype: str

    :raises IOError: If the type of the topic could not be resolved
    """

    time_remaining = timeout

    while True:
        topic_string = rostopic.get_topic_type(topic)[0]

        if topic_string is not None:
            return topic_string
        else:
            if time_remaining <= 0:
                raise IOError("Could not resolve type for topic '{}'. "
                              "Please specify this topic in TOPIC_TYPES or "
                              "run a publisher for this topic.".format(topic))
            time_remaining -= TOPIC_RESOLVE_INTERVAL
            time.sleep(TOPIC_RESOLVE_INTERVAL)


def _try_resolve_topic_type(topic, timeout=0):
    """
    Tries to resolve the type of the specified topic by using the rostopic API and
    dynamic module importing.

    :param topic: The topic to resolve
    :type topic: str
    :param timeout: Number of seconds to retry until raise.
    :type timeout: float

    :returns: The string of the topic type
    :rtype: str

    :raises IOError: If the type of the topic could not be resolved
    """

    # get the string specifying the topic_type
    topic_type_string = _try_resolve_topic_type_string(topic, timeout=timeout)

    """
    Example:

    topic_string: "sensor_msgs/Joy"
    split: ["sensor_msgs", "Joy"]
    package_name: "sensor_msgs.msg"
    class_name: "Joy"
    """

    # extract class_name and package_name
    split = topic_type_string.split("/")
    class_name = split[-1]
    package_name = ".".join(split[:-1]) + ".msg"

    print("class_name: {}".format(class_name))
    print("package_name: {}".format(package_name))

    # import package
    module = importlib.import_module(package_name)

    # get class
    topic_type = getattr(module, class_name)

    return topic_type


def get_topic_type(topic, timeout=TOPIC_RESOLVE_TIMEOUT_DEFAULT):
    """
    Returns the type of the given topic.

    :param topic: The topic from which the type is returned
    :type topic: str
    :param timeout: The time to try to resolve the topic type in seconds
    :type timeout: 
    :returns: The type of the topic
    :rtype: Data type

    :raises IOError: If the type of the topic could not be resolved
    """

    if topic in TOPIC_TYPES:
        topic_type = TOPIC_TYPES[topic]
    else:
        topic_type = _try_resolve_topic_type(topic, timeout=timeout)

    return topic_type
