#!/usr/bin/env python

from threading import Lock, Event

import numpy as np
import rospy
from actions import ActionTransformerRacerTwist
from cv_bridge import CvBridge
from geometry_msgs.msg import TwistStamped
from message_filters import TimeSynchronizer, Subscriber
from neuroracer_ai import AI
from neuroracer_ai.utils.backend_solver import get_backend
from neuroracer_ai.factories.abstract import EBackends
from neuroracer_configclient import ConfigClient
from ruamel.yaml import YAML
from ruamel.yaml.scanner import ScannerError
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import Bool

CONFIGFILE_PATH = "ai_wrapper"
LOCK_TIMEOUT = 1

if get_backend() == EBackends.KERAS:
    from keras.backend.tensorflow_backend import set_session
    import tensorflow as tf
    tf_config = tf.ConfigProto()
    tf_config.gpu_options.allow_growth = True
    set_session(tf.Session(config=tf_config))


class AIWrapper:
    """
    This class uses the neuroracer_ai.AI to predict drive and steering from an camera image.
    The camera image and the activation state is received via a rostopic.
    The output is also send via a rostopic.
    """

    def __init__(self, ai, action_publisher, action_transformer):
        """
        Initiates a AIWrapper instance. Should not be called outside of the AIWrapper class.
        It should be considered private.

        :param ai: An AI instance which is loaded and ready to predict.
        :type ai: neuroracer_ai.AI
        :param action_publisher: A ros publisher to publish the action values predicted by an ai.
        :type action_publisher: rospy.Publisher
        :param action_transformer: A transformer to transform ai output into ros messages
        :type action_transformer: ActionTransformer
        """

        self.ai = ai
        self.action_publisher = action_publisher
        self.action_transformer = action_transformer
        self.active = False
        self.cv_bridge = CvBridge()

        # lock to block the run method until an image is received
        self._image_event = Event()

        # lock to block the image access
        self._image_lock = Lock()
        self._transport_images = None

    @staticmethod
    def load(model_dir, model_name, camera_topics, active_state_topic, action_topic, queue_size=1,
             action_transformer=ActionTransformerRacerTwist(max_drive=1.0)):
        """
        Creates an ai wrapper by loading the specified model.

        :param model_dir: The directory where to search for the model file.
        :type model_dir: str
        :param model_name: The name of the model
        :type model_name: str
        :param camera_topics: The topic from where to receive the camera images
        :type camera_topics: str
        :param active_state_topic: The topic from where to receive the activation state
        :type active_state_topic: str
        :param action_topic: The topic where to publish the predicted actions
        :type action_topic: str
        :param action_transformer: A transformer to transform drive and steer values into a ros message
        :type action_transformer: ActionTransformerRacer
        :param queue_size: queue_size of the publisher for action events
        :type queue_size: int

        :returns: A new AI instance loaded from the specified directory and with the
                  specified name
        :rtype: neuroracer_ai.AI

        :raises IOError: If the specified model doesnt exist
        """

        try:
            ai = AI.load(name=model_name, model_dir=model_dir, trainable=False)
        except IOError:
            raise

        rospy.init_node("neuroracer_ai")

        # publisher to publish the actions predicted by the ai
        action_publisher = rospy.Publisher(action_topic, TwistStamped, queue_size=queue_size)

        # load ai_wrapper
        ai_wrapper = AIWrapper(ai, action_publisher, action_transformer)

        # subscribe_activation events
        rospy.Subscriber(active_state_topic, Bool, ai_wrapper._callback_state)

        # subscribe to camera events in a synchronized way
        # messages need to have header with timestamp --> use stamped messages
        camera_subscribers = [Subscriber(camera_topic, CompressedImage) for camera_topic in camera_topics]
        time_synced_camera_subscriber = TimeSynchronizer(camera_subscribers, queue_size=10)
        time_synced_camera_subscriber.registerCallback(ai_wrapper._callback_camera)

        return ai_wrapper

    @staticmethod
    def check_config(config):
        """
        Raises an KeyError if a needed key could not be found in the config.

        :param config: A configuration dict
        :type config: dict

        :raises KeyError: If a needed key could not be found in the config
        """

        # check configuration
        needed_keys = ["model_name", "model_dir", "camera_topics", "active_state_topic", "action_topic"]
        for key in needed_keys:
            if key not in config:
                raise KeyError("Key '{}' not found in config but needed.".format(key))

    @staticmethod
    def from_configfile(path_to_configfile):
        """
        Loads an AIWrapper with the arguments in the specified configfile

        :param path_to_configfile: The path to the configfile from where to get the configuration.
        :type path_to_configfile: str
        :returns: A new AI instance loaded with the arguments specified in the configfile
        :rtype: neuroracer_ai.AI

        :raises IOError: If the yaml configuration file could not be found
        :raises ScannerError: If the yaml configuration file has invalid syntax
        """

        # load the configfile
        yaml = YAML()
        try:
            with open(path_to_configfile, "r") as f:
                config = yaml.load(f)
        except IOError:
            raise
        except ScannerError:
            raise

        AIWrapper.check_config(config)

        return AIWrapper.load(model_dir=config["model_dir"],
                              model_name=config["model_name"],
                              camera_topics=config["camera_topics"],
                              active_state_topic=config["active_state_topic"],
                              action_topic=config["action_topic"])

    @staticmethod
    def from_configserver(config_path):
        """
        Loads an AIWrapper with the arguments in the specified configfile

        :param config_path: The path to the configfile from where to get the configuration.
        :type config_path: str
        :returns: A new AIWrapper instance loaded with the arguments specified in the configfile
        :rtype: neuroracer_ai_wrapper.AIWrapper
        """

        config_client = ConfigClient()
        config = config_client.get(path=config_path)

        AIWrapper.check_config(config)

        return AIWrapper.load(model_dir=config["model_dir"],
                              model_name=config["model_name"],
                              camera_topics=config["camera_topics"],
                              active_state_topic=config["active_state_topic"],
                              action_topic=config["action_topic"])

    def run(self):
        """
        Starts the AIWrapper.
        Without this call no camera or state messages are received and no drive steer
        messages are send.
        """

        while not rospy.is_shutdown():
            # timeout to check rospy.is_shutdown()
            main_unlocked = self._image_event.wait(timeout=LOCK_TIMEOUT)
            if main_unlocked and self.active:
                # get the image into the main thread
                self._image_lock.acquire()
                images = self._transport_images
                self._image_lock.release()

                if images is None or len(images) == 0 or any([img is None for img in images]):
                    continue

                result = self.ai.predict(images)
                # generate a ros message from the result
                steer, drive = np.squeeze(result)

                message = self.action_transformer.create_action(drive=drive, steer=steer)
                self.action_publisher.publish(message)

    def _callback_state(self, bool_msg):
        """
        Callback to set self.active.

        :param bool_msg: The message containing the information to turn self.active on or off.
        :type bool_msg: rospy.Bool
        """

        if bool_msg.data is True or bool_msg.data is False:
            self.active = bool_msg.data

    def _callback_camera(self, *compressed_image_messages):
        """
        Callback to call whenever a camera message is received.
        This function publishes the action message via ros, if self.active is True.

        :param compressed_image_message: A compressed image message received from a camera
        :type compressed_image_message: sensor_msgs.msg.CompressedImage
        """

        if self.active:
            # extract and add a dimension to image to make it look like a batch with size 1
            # so it fits into ai.predict()
            images = [self.cv_bridge.compressed_imgmsg_to_cv2(compressed_img)
                      for compressed_img in compressed_image_messages if compressed_img is not None]

            self._image_lock.acquire()
            self._transport_images = images
            self._image_lock.release()

            # let the main thread iterate once more
            self._image_event.set()
