#!/usr/bin/env python

import os
import time
import types
from Queue import Queue, Empty

import rosbag
import rospy
from neuroracer_configclient import ConfigClient
from ruamel.yaml import YAML
from ruamel.yaml.scanner import ScannerError
from std_msgs.msg import Bool

from subscribers import AdaptiveSubscriber

PSEUDO_ACTIVE_TOPIC = "ACTIVE_EVENT"
DATE_FORMAT = "%d%m%y_%H%M%S"
QUEUE_TIMEOUT = 1


def topic_to_callback_name(topic):
    """
    Converts a topic into a callback name.

    :param topic: A topic (for example "camera/left/compressed")
    :type topic: str
    :returns: A string which can be used as name for a callback function (for example "camera_left_compressed")
    :rtype: str
    """

    return topic.replace("/", "_") + "_callback"


class Collector:
    def __init__(self, topics, name_format_string, bag_directory):
        """
        Initiates a Collector.  Do not use this method outside this class.
        Better use load() or from_configfile()

        :param topics: The list of topics to collect
        :type topics: List[str]
        :param name_format_string: The formatstring for the bag names
        :type name_format_string: str
        :param bag_directory: Path to the directory where the bagfiles are saved
        :type bag_directory: str
        """

        self.topics = topics

        self.name_format_string = name_format_string
        self.bag_directory = bag_directory

        self.event_queue = Queue()
        self.collecting = False
        self.bag = None

        self.start_button_pressed = False
        self.stop_button_pressed = False

    @staticmethod
    def check_config(config):
        """
        Raises an error if a needed key is not in the config.

        :param config: A dictionary containing configuration information
        :type config: dict

        :raises KeyError: if a needed key is not in the config
        """

        needed_keys = ["topics", "active_topic", "name_format_string", "bag_directory"]

        for key in needed_keys:
            if key not in config:
                raise KeyError("Key '{}' not found in config".format(key))

        if type(list(config["topics"])) is not list:
            raise TypeError("config[\"topics\"] must be a list but is of type '{}'"
                            .format(type(config["topics"])))

    @staticmethod
    def create(topics, name_format_string, bag_directory, active_topic):
        """
        Creates a new Collector.

        :param topics: A list of topics to collect
        :type topics: List[str]
        :param name_format_string: A format string specifying the names of the created rosbags
        :type name_format_string: str
        :param bag_directory: The directory where to save the rosbags.
        :type bag_directory: str
        :param active_topic: The topic from where to receive start and stop commands
        :type active_topic: str
        """

        rospy.init_node("collector")

        collector = Collector(topics=topics,
                              name_format_string=name_format_string,
                              bag_directory=bag_directory)

        # Subscribe active messages
        rospy.Subscriber(active_topic,
                         Bool,
                         collector.active_callback)

        # Subscribe data topics
        for topic in topics:
            # add callback dynamic to collector
            c = collector.create_callback(topic)

            AdaptiveSubscriber(name=topic,
                               callback=c)

        return collector

    @staticmethod
    def from_configfile(configfile_path):
        """
        Loads a Collector with the arguments specified in the configfile

        :param configfile_path: Path to a configfile
        :type configfile_path: str
        :returns: A new Collector initialized with the data in the configfile
        :rtype: Collector
        """

        # load configfile
        yaml = YAML()
        try:
            with open(configfile_path, "r") as f:
                config = yaml.load(f)
        except IOError:
            raise IOError("Could not load configfile: '{}'".format(configfile_path))
        except ScannerError:
            raise IOError("Could not parse configfile: '{}'".format(configfile_path))

        Collector.check_config(config)

        collector = Collector.create(topics=list(config["topics"]),
                                     name_format_string=config["name_format_string"],
                                     bag_directory=config["bag_directory"],
                                     active_topic=config["active_topic"])

        return collector

    @staticmethod
    def from_configserver(config_path):
        """
        Loads a Collector with the arguments specified by the configserver

        :param config_path: Path to a configfile
        :type config_path: str
        :returns: A new Collector initialized with the data in the configfile
        :rtype: Collector
        """

        config_client = ConfigClient()
        config = config_client.get(path=config_path)

        collector = Collector.create(topics=list(config["topics"]),
                                     name_format_string=config["name_format_string"],
                                     bag_directory=config["bag_directory"],
                                     active_topic=config["active_topic"])

        return collector

    def start_collecting(self):
        """
        Initiates a bag and enables writing to this bag.
        If there is a bag being written in right now, the old bag is
        closed and a new bag is started.
        """

        # close old bag if existing
        self.close_bag()

        # initiate new bag
        timestamp_string = time.strftime(DATE_FORMAT)
        bag_name = self.name_format_string.format(timestamp=timestamp_string)

        filename = os.path.join(self.bag_directory, "{bag_name}.bag".format(bag_name=bag_name))

        i = 0
        while os.path.isfile(filename):
            identifier = "" if i == 0 else "_{}".format(i)
            filename = os.path.join(self.bag_directory, "{bag_name}{identifier}.bag".format(bag_name=bag_name,
                                                                                            identifier=identifier))
            i += 1

        self.bag = rosbag.Bag(filename, 'w')

        # enable writing data into this bag
        self.collecting = True

    def close_bag(self):
        """
        Closes the currently used bag if existing.
        """

        if self.bag is not None:
            self.bag.close()
            self.bag = None

    def stop_collecting(self):
        """
        Stops the process of writing data into the current bag and closes the bag.
        """

        self.collecting = False
        self.close_bag()

    def run(self):
        """
        Runs the Collector. Without this call no events are collected.
        """

        while not rospy.is_shutdown():
            try:
                # timeout to check rospy.is_shutdown()
                (topic, msg) = self.event_queue.get(timeout=QUEUE_TIMEOUT)
            except Empty:
                continue  # if no events in the queue, continue waiting

            # turn collecting on or off
            if topic == PSEUDO_ACTIVE_TOPIC:
                if msg == "start":
                    self.start_collecting()
                elif msg == "stop":
                    self.stop_collecting()
                else:
                    raise ValueError("Unknown msg for active topic: '{}'".format(msg))
            else:
                if self.collecting and self.bag is not None:
                    self.bag.write(topic, msg)

        self.close_bag()

    def create_callback(self, topic):
        """
        Creates a callback for this Collector instance.

        :param topic: The topic for which a callback should be created
        :type topic: str
        :returns: A reference to the callback
        :rtype: types.MethodType
        """

        # this callback adds a tuple (topic, msg) into the event queue
        def callback(self_callback, msg):
            if self_callback.collecting:
                self_callback.event_queue.put((topic, msg))

        # load new callback for this Collector instance
        c = types.MethodType(callback, self)

        # add the callback to this Collector instance with the right name
        self.__dict__[topic_to_callback_name(topic)] = c
        return c

    def active_callback(self, message):
        # stop button
        message_text = "start" if message.data else "stop"

        # use the PSEUDO_ACTIVE_TOPIC to move the start/stop information into the main thread
        self.event_queue.put((PSEUDO_ACTIVE_TOPIC, message_text))
