#!/usr/bin/env python

from __future__ import division
from __future__ import print_function

import rospy
from geometry_msgs.msg import TwistStamped
from neuroracer_configclient import ConfigClient
from sensor_msgs.msg import Joy
from std_msgs.msg import Bool

from devices.gamepad import JoyMsgMapper


class Operator:
    """
    State management class for the whole robot. It combines the internal robot
    state with the user input from the gamepad.
    """

    def __init__(self, engine_input_publisher, ai_active_publisher,
                 collector_active_publisher, joy_mapping):
        """
        :param engine_input_publisher:
        :type engine_input_publisher: rospy.Publisher
        :param ai_active_publisher:
        :type ai_active_publisher: rospy.Publisher
        :param collector_active_publisher:
        :type collector_active_publisher: rospy.Publisher
        :param joy_mapping: Dict[str, Dict]
        """

        self.engine_input_publisher = engine_input_publisher
        self.ai_active_publisher = ai_active_publisher
        self.collector_active_publisher = collector_active_publisher
        self.joy_mapper = JoyMsgMapper.from_config(joy_mapping_dict=joy_mapping)
        self.ai_is_active = False
        self._old_joy_input = None

        # for wrong Rosjoy trigger value fix
        self._joy_mapping_config = joy_mapping
        self._joy_trigger_l_was_pressed = False
        self._joy_trigger_r_was_pressed = False

    @staticmethod
    def create(config):
        """
        Factory method to load a Operator instance based on a given config
        dict (usually from the config server).

        :param config: config dict
        :type config: Dict[str, Dict]
        :return: new Operator instance
        :rtype: Operator
        """

        rospy.init_node("operator")

        engine_input_publisher = rospy.Publisher(name=config["engine_input_topic"],
                                                 data_class=TwistStamped,
                                                 queue_size=1)
        ai_active_publisher = rospy.Publisher(name=config["ai_input_topic"],
                                              data_class=Bool,
                                              queue_size=1)
        collector_active_publisher = rospy.Publisher(name=config["collector_input_topic"],
                                                     data_class=Bool,
                                                     queue_size=1)

        operator = Operator(engine_input_publisher=engine_input_publisher,
                            ai_active_publisher=ai_active_publisher,
                            collector_active_publisher=collector_active_publisher,
                            joy_mapping=config["joy_mapping"])

        rospy.Subscriber(name=config["joy_output_topic"],
                         data_class=Joy,
                         callback=operator._callback_joy)
        rospy.Subscriber(name=config["ai_action_output_topic"],
                         data_class=TwistStamped,
                         callback=operator._callback_ai)

        return operator

    @staticmethod
    def from_configserver(config_path):
        """
        Factory method to load a Operator instance with a config from the
        configserver.

        :param config_path: configserver path to the config entry
        :type config_path: list [str]
        :return: new Operator instance
        :rtype: Operator
        """

        config_client = ConfigClient()
        config = {}

        for path in config_path:
            config.update(config_client.get(path=path))

        return Operator.create(config)

    def _callback_joy(self, raw_joy_command):
        """
        Internal callback function for the ros joy node.

        :param raw_joy_command: ros joy msg
        :type raw_joy_command: sensor_msgs.msg.Joy
        """

        # FIX for Rosjoy Left_Trigger and Right_Trigger BUG
        # sets trigger values to 1.0 if not already pressed
        # because default the value is 0.0 but should be 1.0

        if not self._joy_trigger_l_was_pressed:
            reverse_index = self._joy_mapping_config["drive"]["parts"][0]["index"]
            l_trigger_value = raw_joy_command.axes[reverse_index]
            if l_trigger_value == 0.0:
                raw_joy_command.axes = list(raw_joy_command.axes)
                raw_joy_command.axes[reverse_index] = 1.0
                raw_joy_command.axes = tuple(raw_joy_command.axes)
            else:
                self._joy_trigger_l_was_pressed = True

        if not self._joy_trigger_r_was_pressed:
            forward_index = self._joy_mapping_config["drive"]["parts"][1]["index"]
            r_trigger_value = raw_joy_command.axes[forward_index]
            if r_trigger_value == 0.0:
                raw_joy_command.axes = list(raw_joy_command.axes)
                raw_joy_command.axes[forward_index] = 1.0
                raw_joy_command.axes = tuple(raw_joy_command.axes)
            else:
                self._joy_trigger_r_was_pressed = True

        joy_input = self.joy_mapper.remap_raw_input(raw_joy_input=raw_joy_command)

        if joy_input.has_ai_state_changed(self._old_joy_input):
            self.ai_is_active = joy_input.is_ai_active()
            self.ai_active_publisher.publish(joy_input.is_ai_active())

        if joy_input.has_collector_state_changed(self._old_joy_input):
            self.collector_active_publisher.publish(joy_input.is_collector_active())

        self.engine_input_publisher.publish(joy_input.get_manual_action())

        self._old_joy_input = joy_input

    def _callback_ai(self, action):
        """
        Internal callback function for the ai node.

        :param action: predicted ai action
        :type action: geometry_msgs.msg.TwistStamped
        """

        if self.ai_is_active:
            self.engine_input_publisher.publish(action)

    def run(self):
        """
        Lets the operator spin until a ros shutdown happens.
        """

        rospy.spin()
